package ro.mihaaiiii.quests;

public class Question3 {
    /*
        Define an integer variable called bankBalance. Initialize it to a value of 500. Then add 250
        to it. Then subtract 100 from it. Finally, print the resulting value.

    */
    public static void main(String[] args) {
        int bankBalance = 500;
        bankBalance += 250 - 100;

        System.out.println(bankBalance);
    }
}
