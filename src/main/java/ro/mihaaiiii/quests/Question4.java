package ro.mihaaiiii.quests;

public class Question4 {
    /*
        What value will be printed by this line of Java code?
        System.out.println(2.0 * (5 / 2));
            A. 4
            B. 4.0
            C. 5
            D. 5.0
            E. This line of code will give an error.
    */
    public static void main(String[] args) {
        System.out.println(2.0 * (float)(5 / 2)); //B
    }
}
