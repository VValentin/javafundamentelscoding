package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task2 {

    public static void main(String[] args) {
        float weight;
        int height;
        System.out.println("Introduceti greutatea in kg: ");
        weight = ScannerUtils.readFloatFromUser();
        System.out.println("Introduceti inaltimea in cm: ");
        height = ScannerUtils.readIntFromUser();
        double bmi = weight / (((double) height / 100) * (height / 100.0));
        System.out.println("Indicele de masa corporala este: " + bmi);
        if (bmi >= 18.5 && bmi <= 24.9) {
            System.out.println("Indicele de masa corporala este optim.");
        } else {
            System.out.println("Indicele de masa corporala nu este optim.");
        }
    }
}

