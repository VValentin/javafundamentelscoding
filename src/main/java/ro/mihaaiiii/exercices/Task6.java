package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task6 {
    public static void main(String[] args) {
        System.out.println("Enter a number:");
        int number = ScannerUtils.readIntFromUser();
        double harmonicSum = 0;

        for (int i = 1; i <= number; i++) {
            harmonicSum =harmonicSum + (1.0 /i);
            System.out.println(harmonicSum);
        }
        System.out.printf("Sum Harmonic is %.2f ",harmonicSum);
    }

}
