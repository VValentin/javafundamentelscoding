package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task3 {
    public static void main(String[] args) {
        int a, b, c, delta;
        System.out.println("Intorduceti a: ");
        a = ScannerUtils.readIntFromUser();
        System.out.println("Intorduceti b: ");
        b = ScannerUtils.readIntFromUser();
        System.out.println("Intorduceti c: ");
        c = ScannerUtils.readIntFromUser();


        delta = (b * b) - (a * a * c);
        if (delta < 0) {
            System.out.println("Delta negativ");
            return;
        }
        double root1, root2;
        root1 = ((-b - Math.sqrt(delta)) / (2*a));
        root2 = ((-b + Math.sqrt(delta)) / (2*a));

        System.out.printf("X1= %.4f  X2: %.4f ", + root1 , root2);
    }


}
