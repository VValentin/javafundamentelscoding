package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task10 {
    public static void main(String[] args) {
        System.out.println("Introduceti un unmar: ");
        int number = ScannerUtils.readIntFromUser();
        int sumOfDigits = 0;
        while (number > 0) {
            sumOfDigits += (number % 10);
            number = number / 10;

        }
        System.out.println(sumOfDigits );

    }


}
