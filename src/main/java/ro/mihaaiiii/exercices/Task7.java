package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task7 {

    public static void main(String[] args) {
        System.out.println("Enter a positive number:");
        int number = ScannerUtils.readIntFromUser();

        if (number == 0 || number == 1) {
            System.out.println(1);
            return;
        }
        int previosNumber = 1;
        int lastNumber = 1;
        int fibonaciiNumber = 0;
        for (int i = 2; i <= number; i++) {

            fibonaciiNumber = previosNumber + lastNumber;
            previosNumber = lastNumber;
            lastNumber = fibonaciiNumber;
        }
        System.out.println(fibonaciiNumber);
    }

}
